Contractor Timesheet Generator
==============================

For Open Office and Libre Office. Tested in versions: LO 4.5 and AOO 4.1.1
Writtent in Open Office Basic (for now)

Pregenerates timesheet for given months.  
Customizable template using embedded Calc sheet 
It fills all working days (week days, no integration with public holidays, sorry)  
It creates a new document with the given months timesheet to prevent timesheet template being overriden.  
The new document does not contain original macro.  
User is then expected to correct - add or remove worked days - from the newly created document.  

Still beta quality but usable.

![app-screenshot-1](./docs/contractor-timesheet-generator-screenshot.png "Template when it is opened, ready to be customized, ready to generate")

[More screenshots, more documentation](./docs/readme.md).

TODO
====

 * improve Basic code, prune unused code, more documentation
 * improve documentation, add screenshots
 * fix various remaining issues:
    * due to issue with gotoStart() document must start with a text, so I had to add tiny characted, a space, so in fact invisible, at the very beginning of the document. 
    * original cursor position is not remebered but always set to top left corner of the document 
    * remove helper hidden template sheet from the generated document
 * add ability to set output directory - for now it saves new document always in same directory as template document. 
 * rewrite in Java
 * check if by any chance this works in MS Office too

See
===

 * https://www.libreoffice.org/ - Libre Office suite
 * https://www.openoffice.org/ - Apache Open Office, you need one of those to run Contractor Timesheet 
 * https://forum.openoffice.org/en/forum/viewtopic.php?f=21&t=78814 - Copy full content from one document to another - my snipplet published at OO Forum 
 * https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=73418 - Relative instead of Absolute URL path
 * https://bugs.documentfoundation.org/show_bug.cgi?id=93944 - gotoStart does not work in a writer document starting with table
 * https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=79065 - gotoStart does not work in document starting with table
 * https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=78759 - Minor issue with copying content from file to file; where origDocument.lockControllers() and unlockControllers() were suggested and helped
 * https://wiki.openoffice.org/wiki/Writer/API/Text_cursor - text cursors, view cursors 
 * https://wiki.openoffice.org/wiki/Documentation/BASIC_Guide/Strings_%28Runtime_Library%29 - Strings (OpenOffice.org BASIC Runtime Library)
 * https://wiki.openoffice.org/wiki/Documentation/BASIC_Guide/StarDesktop - where loadComponentFromURL() and storeAsURL() are described