''' This is exported Open Office Basic code (macro) from 'ContractorsTimesheetTemplate.odt' for convenience
''' Main procedure, entry point for generating timesheet
'''
Sub GenerateTimesheet
  Dim s As Object
  Dim baseUri
  
  baseUri = getDocumentFolder(ThisComponent)
  
  deleteSpreadsheetIfExistsByName("Sheet1")
  ' create new Sheet1 using Teplate sheet, insert new sheet to position 0, as the first one
  ThisComponent.EmbeddedObjects(0).Component.sheets.copyByName("Template", "Sheet1", 0)
  
  s = ThisComponent.EmbeddedObjects(0).Component.sheets.getByName("Sheet1")
  
  Dim projectCode
  projectCode = getProjectCodeFromTable()
  
  Dim yearMonth as String
  yearMonth = getYearMonthFromTable()
  Dim inputYear, inputMonth
  inputYear  = Left(yearMonth, 4)
  inputMonth = Right(yearMonth, 2)

  Dim startDate
  startDate = DateSerial(CInt(inputYear), CInt(inputMonth), 1)

  Dim numberOdDaysInMonth
  numberOdDaysInMonth = numberOfDaysInMonthFunc(startDate)
  
  Dim endDate
  endDate =  DateSerial(CInt(inputYear), CInt(inputMonth), numberOdDaysInMonth)
  
  fillFromToDatesToTable(startDate, endDate)
  
  Dim sumRow
  sumRow = numberOdDaysInMonth + 1
  
  s.rows.insertByIndex(2, numberOdDaysInMonth)
  
  for i = 0 to (numberOdDaysInMonth - 1)
    Dim d, weekDayName, weekDayNo, row
    
    row = i + 2
    
    d = DateAdd("d", i, startDate) 
    weekDayName = Format(d, "dddd") ' Monday, Tuesday, .. Sunday
    weekDayNo = WeekDay(d)
    
    s.getCellbyPosition(0, row).String = Format(d, "d/m")
    s.getCellbyPosition(2, row).String = weekDayName
      
    if isWeekend(weekDayNo) then
       s.getCellbyPosition(3, row).setValue(1)
    end if
      
    ' draw line under Sunday (end of week separator)
    if isEndOfWeek(weekDayNo) then
       drawLine(s, row)
    end if
  Next
  
  'remove helping rows
  s.rows.removeByIndex(1, 1)
  s.rows.removeByIndex(1 + numberOdDaysInMonth, 1)
  
  ' embedded object occasionally does not refresh so I am enforcing it; refresh whole document, just in case
  thisComponent.refresh
  
  Dim newDoc
  newDoc = copyContentToNewFile()
  saveNewDocument(newDoc, baseUri, "timesheet", projectCode, yearMonth)
End Sub


' Prevent 
' BASIC runtime error: com.sun.star.container.NoSuchElementException
Sub deleteSpreadsheetIfExistsByName(sheetName) 
   On Error Resume Next ' ignore exceptions in this whole procedure
   ThisComponent.EmbeddedObjects(0).Component.sheets.removeByName(sheetName)
   ' ..or use hasByName(sheetName) to check if sheet of given name exists and delete only when it does
   ' http://www.oooforum.org/forum/viewtopic.phtml?t=3613
End Sub


Sub drawHeaderRow(s As Object)
   dim row
   row = 0
   drawUpperAndLowerLine(s, row, 5)
   s.getCellbyPosition(0, row).String = "Date"
   s.getCellbyPosition(0, row).horiJustify = 1
   s.getCellbyPosition(1, row).String = " "
   s.getCellbyPosition(2, row).String = "Day"
   s.getCellbyPosition(2, row).horiJustify = 1
   s.getCellbyPosition(3, row).String = "Worked (days)"
   s.getCellbyPosition(3, row).horiJustify = 2
End Sub


Function isWeekend(weekDayNo) as Boolean
   if weekDayNo >= 2 and weekDayNo <= 6 then
      isWeekend = True
   else
      isWeekend = False
   end if
End Function


Function isEndOfWeek(weekDayNo) as Boolean
   if weekDayNo = 1 then
      isEndOfWeek = True
   else
      isEndOfWeek = False
   end if
End Function


Sub drawLine(s As Object, row)
   Dim basicBorder as New com.sun.star.table.BorderLine
   Dim oBorder As Object
   oBorder = s.getCellByPosition(0,row).TableBorder
   basicBorder.Color = RGB(0, 0, 0)
   basicBorder.OuterLineWidth = 0.5
   oBorder.BottomLine = basicBorder
   s.getCellRangeByPosition(0,row,10,row).TableBorder = oBorder
End Sub


Sub drawUpperLine(s As Object, row)
   Dim basicBorder as New com.sun.star.table.BorderLine
   Dim oBorder As Object
   oBorder = s.getCellByPosition(0,row).TableBorder
   basicBorder.Color = RGB(0, 0, 0)
   basicBorder.OuterLineWidth = 0.5
   oBorder.TopLine = basicBorder
   s.getCellRangeByPosition(0,row,10,row).TableBorder = oBorder
End Sub


' Set visible border lines for given row of cells - both upper and lower line
' s .. spreadsheet
' row .. index row
' columns .. number of columns; length 
Sub drawUpperAndLowerLine(s As Object, row As long, columns As long)
   dim lineStyle as new com.sun.star.table.BorderLine
   dim cellBorder as Object
   cellBorder = s.getCellByPosition(0,row).tableBorder
   lineStyle.Color = RGB(0, 0, 0)
   lineStyle.OuterLineWidth = 0.5
   cellBorder.topLine = lineStyle
   cellBorder.bottomLine = lineStyle
   s.getCellRangeByPosition(0,row,columns,row).tableBorder = cellBorder
End Sub


Sub drawSumLine(s as Object, sumRow)
   drawUpperLine(s, sumRow)
   s.getCellbyPosition(0, sumRow).String = " "
   s.getCellbyPosition(2, sumRow).String = "Days in total"
   s.getCellbyPosition(3, sumRow).setFormula("=SUM(D2:D" + sumRow + ")")
End Sub 


' month must be a long (numerical) form of a whole date, like 2015-01-01 is 42005
' obscurdly DAYSINMONTH when called via FunctionAccess requires array of numbers, not date or dates!
' OO Basic does have a Date type 
' See: http://www.oooforum.org/forum/viewtopic.phtml?t=20381
Function numberOfDaysInMonthFunc(month as Long) As Integer
   Dim functionAccess 
   functionAccess = createUnoService("com.sun.star.sheet.FunctionAccess")
   numberOfDaysInMonthFunc = functionAccess.callFunction("DAYSINMONTH", array(month))
End Function


Function getYearMonthFromTable() as String
   Dim textTable, r 
   textTable1 = ThisComponent.textTables(0)
   r = textTable1.getCellbyPosition(4, 1).String
   getYearMonthFromTable = r
End Function


Function getProjectCodeFromTable() as String
   Dim textTable, r 
   textTable1 = ThisComponent.textTables(0)
   r = textTable1.getCellbyPosition(2, 4).String
   getProjectCodeFromTable = r
End Function


' fill "1/1/2015 - 31/1/2015 (January)" to text table [0] cell [2, 3]
Sub fillFromToDatesToTable(startDate as Date, endDate as Date) as String
   Dim textTable, cell, d1, d2, d3 
   textTable1 = ThisComponent.textTables(0)
   cell = textTable1.getCellbyPosition(2, 3)
   d1 = Format(startDate, "dd/mm/yyyy")
   d2 = Format(endDate, "dd/mm/yyyy")
   d3 = Format(endDate, "mmmm")
   cell.setString(d1 + " - " + d2 + " (" + d3 + ")")
End Sub


''' Copy content from one document to another document; 
''' select all and copy, open new blank unnamed document and paste all the selected content from first document
Function copyContentToNewFile() As Object
	dim c1, c2, newDoc, selectedContent, origPosition, origPositionTC
	
	origDocument = ThisComponent
	
	
	
	origDocument.lockControllers()
	
	' preserve original position, when working with controller selection otherwise it gets lost
	origPosition = origDocument.CurrentController.getViewCursor()
	origPosition.jumpToFirstPage(false) ' hack to avoid cursor being in table which would cause Error: End of content node doesn't have the proper start node.
	origPosition.jumpToStartOfPage(false)
	origPositionTC = origDocument.getText.createTextCursorByRange(origPosition)
 
    ' select all in original document
	c1 = origDocument.text.createTextCursor
	c1.gotoStart(false)	
	c1.gotoEnd(true)
	
	' copy selected from original document
	origDocument.CurrentController.select(c1)            ' select content of the whole document, from the start to the end; here cursor represents whole text range, selected content of a document 
	selectedContent = origDocument.CurrentController.getTransferable() ' copy the content for later use 
		
	' create new blank document
	newDoc = StarDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, Array())	'Open blank Writer doc
	
	' paste selected to the new document, on cursor position
	c2 = newDoc.text.createTextCursor ' 
	c2.gotoStart(false)
	newDoc.CurrentController.select(c2)  ' set insertion point, represented by cursor C2, in the new file at the document start; here cursor represents pointer to a position, not a range
	newDoc.CurrentController.insertTransferable(selectedContent)
	
	' deselect current selection; current selection is whole document by now and it is visible selection
	' return cursor to original position; and selection to original selection if any was present before running macro	
	origDocument.CurrentController.select(origPositionTC) 
	
	' set view curson in the new document at the beginning of document
	' not essential, feels more 'intuitive' behaviour
	newDoc.currentController.viewCursor.gotoStart(false)
	
	origDocument.unlockControllers()
	
	copyContentToNewFile = newDoc
End Function


' Attempt to save the new document using Project Code, date
' TODO: remove macros and template sheet from the new document
' prefix - 'timesheet' is suggested
' projectCode - good to differentiate espetially if one have to fill multiple timesheets per month
' dateCode - example '2015-09' for September's timesheet 
Sub saveNewDocument(newDoc, baseUri, prefix, projectCode, dateCode) 
	Dim url
	Dim newFileProperties() 'An (empty) array of PropertyValues
	url = baseUri + prefix + "-" + projectCode + "-" + dateCode + ".odt"
	newDoc.storeAsURL(url, newFileProperties)
End Sub


' Returns give document containing folder
' Example: for 'file:///c:/home/myDocs/doc123.odt returns file:///c:/home/myDocs'
Function getDocumentFolder(doc As Object) As String
  Dim pathString, pathLength, slashPos, lastSlashPos
  if doc.hasLocation then
     pathString = doc.getURL()
     pathLength = len(pathString)
     slashPos = InStr(1, pathString, "/")
     do while slashPos > 0
       lastSlashPos = slashPos
       slashPos = InStr(slashPos + 1, pathString, "/")
     loop
     getDocumentFolder = Left(pathString, lastSlashPos)
  else
     Print "Error - No location yet!"
  end if
End Function
