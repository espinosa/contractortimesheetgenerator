Contractor Timesheet Generator
==============================

More screenhots to get you the idea how this thing works:

How it all looks, template itself:  
![app-screenshot-1](./contractor-timesheet-generator-screenshot.png "Template when it is opened, ready to be customized, ready to generate")

Launch generator from menu. It is a custom menu in main menu.
Also crucial is to set month and year to the table header; 2015-08 stands indeed for August 2015.
![app-screenshot-2](./launch-generator-from-menu.png "Fill year and date first then launch generator from custom menu")  
Note: the custom menu appears only for this document. 

New file is generated and opened and put forward. It is also pre-saved with name reflecting project code and date a prefix 'timesheet'.

Amend generated timesheet document. Generated timesheet document still contains editable embedded Calc sheet with sum field, automacically recalculated after every change:   
![app-screenshot-3](./amend-generated-timesheet.png "Amend generated timesheet document. Fill missing days, remove those not worked. Total sum is auto recalulated")

Inner table teplate, hidden template sheet in embedded cals. If you want to change anything regarding the table header, or summary field, or font or size of individual rows, do it conveniently here:  
![app-screenshot-4](./hidden-template-sheet.png "Inner time sheet template, normally hiden behind the day by day sheet, or sheet1 by name")

When document opens, user is usually presented with a warning about macros. Macros have to be allowed or the generator obviously will not work.  
![app-screenshot-5](./dont-be-scared-allow-macros.png "Please allow macros")
